//
//  MainController.swift
//  sampleProject
//
//  Created by Ragul on 14/01/20.
//  Copyright © 2020 Ragul. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData

class MainController: UIViewController {
    
    @IBOutlet weak var openIssuesButton: UIButton!
    @IBOutlet weak var closeIssuesButton: UIButton!
    @IBOutlet weak var issuseCollectionView: UICollectionView!
    @IBOutlet weak var stackView: UIStackView!
    
    var issuesArr:[MainDataEntity] = []
    var openIssuesArr:[MainDataEntity] = []
    var closeIssuesArr:[MainDataEntity] = []
    
    //MARK:- Local Variable
    //Get current page index
    var currentPage = 0 {
        didSet {
            UIView.animate(withDuration: 0.1) {
                if 0 == self.currentPage {
                    self.openIssuesButton.setTitleColor(.green, for: .normal)
                    self.closeIssuesButton.setTitleColor(.black, for: .normal)
                }else{
                    self.openIssuesButton.setTitleColor(.black, for: .normal)
                    self.closeIssuesButton.setTitleColor(.green, for: .normal)
                    
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initalLoads()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func initalLoads(){
        
        setColors()
        setTitle()
        registerCollectionView()
        currentPage = 0
        fetchTrendingDataReq()
        setButtonActions()
    }
    
    private func registerCollectionView(){
        issuseCollectionView.delegate = self
        issuseCollectionView.dataSource = self
        issuseCollectionView.register(nibName: Indentifier.MainCollectionViewCell)
    }
    private func setButtonActions(){
        openIssuesButton.addTarget(self, action: #selector(tapOpenIssues), for: .touchUpInside)
        closeIssuesButton.addTarget(self, action: #selector(tapCloseIssues), for: .touchUpInside)
        
    }
    
    private func setColors(){
        openIssuesButton.backgroundColor = .white
        closeIssuesButton.backgroundColor = .white
        closeIssuesButton.setTitleColor(.black, for: .normal)
        openIssuesButton.setTitleColor(.green, for: .normal)
        self.view.backgroundColor = .groupTableViewBackground
    }
    private func setTitle(){
        openIssuesButton.setTitle(Constants.openIssues, for: .normal)
        closeIssuesButton.setTitle(Constants.closeIssues, for: .normal)
    }
    @objc func tapOpenIssues() {
        self.currentPage = openIssuesButton.tag
        
        self.issuseCollectionView.reloadData()
        
    }
    @objc func tapCloseIssues() {
        self.currentPage = closeIssuesButton.tag
        self.issuseCollectionView.reloadData()
        
        
    }
    
}
extension MainController : PresenterOutputProtocol {
    func showError(error: CustomError) {
        print(error)
        
    }
    
    func showSuccess(dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        if String(describing: modelClass) == model.type.MainDataEntity {
            self.issuesArr = dataArray as? [MainDataEntity] ?? []
            
            if issuesArr.count > 0 {
                for data in issuesArr {
                    let issueList = MainEntity.fetchRequest(for: Int64(data.id ?? 0))
                    issueList.populateWithRepresentation(representation:data)
                    DataBaseManager.shared.saveContext()
                }
            }
            
            for data in issuesArr {
                if data.state == "open" {
                    openIssuesArr.append(data)
                }else{
                    closeIssuesArr.append(data)
                    
                }
            }
            self.issuseCollectionView.reloadData()
        }
        
    }
    
    func fetchTrendingDataReq() {
        guard let suggestedNews = try! DataBaseManager.shared.context.fetch(MainEntity.fetchRequest()) as? [MainEntity] else { return }
        if suggestedNews.count > 0 {
            for data in suggestedNews {
                var customObj = MainDataEntity()
                
                customObj.id = data.id
                customObj.body = data.body
                customObj.title = data.title
                customObj.number = data.number
                customObj.state = data.state
                customObj.user?.login = data.login
                customObj.created_at = data.created_at
                self.issuesArr.append(customObj)
            }
            for data in issuesArr {
                if data.state == "open" {
                    openIssuesArr.append(data)
                }else{
                    closeIssuesArr.append(data)
                    
                }
            }
            self.issuseCollectionView.reloadData()
            
        }else{
            self.presenter?.GETPOST(api: EndPointUrl.getAlamorFireList, params: [:], methodType: .GET, modelClass: MainDataEntity.self, token: false)
            
        }
    }
    
    
    
}

extension MainController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:MainCollectionViewCell = issuseCollectionView.dequeueReusableCell(withReuseIdentifier: Indentifier.MainCollectionViewCell, for: indexPath) as! MainCollectionViewCell
        if currentPage == 0 {
            cell.setData(data: openIssuesArr)
            
        }else{
            cell.setData(data: closeIssuesArr)
            
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: issuseCollectionView.frame.width, height: issuseCollectionView.frame.height)
    }
    
    
}
extension MainController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        self.currentPage = currentPage
        self.issuseCollectionView.reloadData()
        
    }
}
