//
//  MainCollectionViewCell.swift
//  sampleProject
//
//  Created by Ragul on 14/01/20.
//  Copyright © 2020 Ragul. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subIssuesTableView: UITableView!

    var issuesArr:[MainDataEntity] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initalLoad()
    }
    
    private func initalLoad(){
        subIssuesTableView.delegate = self
        subIssuesTableView.dataSource = self
        subIssuesTableView.register(nibName: Indentifier.MainSubIssueCell)
    }
    func setData(data: [MainDataEntity]){
        
        issuesArr = data
       
        subIssuesTableView.reloadData()

    }

}
extension MainCollectionViewCell : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = issuesArr[indexPath.row]
        let vc = WebViewController()
        vc.urlString = dict.body ?? ""
        (UIApplication.topViewController() as AnyObject).navigationController?.pushViewController(vc, animated: true)

    }
}
extension MainCollectionViewCell : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issuesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Indentifier.MainSubIssueCell, for: indexPath) as! MainSubIssueCell
        let dict = issuesArr[indexPath.row]
        cell.titleLabel.text = dict.title
        let numb = "#" + (dict.number?.toString() ?? "") + " "
        let name = (dict.user?.login ?? "") + " "
        cell.subTitleLabel.text = numb + name + (dict.created_at ?? "")
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
