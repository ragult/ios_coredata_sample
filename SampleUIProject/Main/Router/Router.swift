//
//  Router.swift
//  sampleProject
//
//  Created by Ragul on 14/01/20.
//  Copyright © 2020 Ragul. All rights reserved.
//

import Foundation
import UIKit

var presenterObject :PresenterInputProtocol?

class Router: PresenterToRouterProtocol {
    static let main = UIStoryboard(name: "Main", bundle: Bundle.main)

    
    static func createModule()->(UIViewController){
        
        let mainPresenter : PresenterInputProtocol & InterectorToPresenterProtocol = Presenter()
        let mainInteractor : PresenterToInterectorProtocol & WebServiceToInteractor = Interactor()
        let mainRouter : PresenterToRouterProtocol = Router()
        let mainWebservice : WebServiceProtocol = Webservice()

        
        if let view : (PresenterOutputProtocol & UIViewController) = main.instantiateViewController(withIdentifier: Indentifier.MainController) as? MainController {
            
            mainPresenter.view = view
            view.presenter = mainPresenter
            presenterObject = view.presenter
            
        }
        
       
        mainPresenter.interactor = mainInteractor
        mainPresenter.router = mainRouter
        mainInteractor.presenter = mainPresenter
        mainInteractor.webService = mainWebservice
        mainWebservice.interactor = mainInteractor
        
                let nav = UINavigationController(rootViewController: main.instantiateViewController(withIdentifier:  Indentifier.MainController))
                nav.isNavigationBarHidden = true
                return nav
    }
    
}
