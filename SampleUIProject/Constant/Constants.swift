//
//  Constants.swift
//  sampleProject
//
//  Created by Ragul on 14/01/20.
//  Copyright © 2020 Ragul. All rights reserved.
//

import UIKit

enum Indentifier {
    static let MainController = "MainController"
    static let MainCollectionViewCell = "MainCollectionViewCell"
    static let MainSubIssueCell = "MainSubIssueCell"
}
enum Constants {
    static let openIssues = "Open Issues"
    static let closeIssues = "Close Issues"
    static let noNetwork = "Please check your Internet conncection"
    
    //Content Type
    static let RequestType = "X-Requested-With"
    static let RequestValue = "XMLHttpRequest"
    static let ContentType = "Content-Type"
    static let ContentValue = "application/json"
    static let Authorization = "Authorization"
    static let MultiPartValue = "multipart/form-data"
    static let Bearer = "Bearer"
    static let multiPartValue = "multipart/form-data"
    static let errorCode = "statusCode"
    
}

enum ImageString {
    static let error = "ic_error"
}

struct EndPointUrl {
    static let getAlamorFireList = "https://api.github.com/repos/alamofire/alamofire/issues?state=all"
}

//MARK:- HTTP Methods

enum HttpType : String {
    
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case PUT = "PUT"
    case DELETE = "DELETE"
    
}

//MARK:- Web Constants:
struct WebConstants {
    static let string = WebConstants()
    let get = "GET"
    let post = "POST"
    let secretKey = "secretKey"
    let X_Requested_With = "X-Requested-With"
    let XMLHttpRequest = "XMLHttpRequest"
    let authorization = "Authorization"
    let Content_Type = "Content-Type"
    let application_json = "application/json"
    let multipartFormData = "multipart/form-data"
    let Authorization = "Authorization"
    let token_type = "token_type"
    
    
}
//MARK:- Status Code

enum StatusCode : Int {
    
    case notreachable = 0
    case success = 200
    case multipleResponse = 300
    case unAuthorized = 401
    case notFound = 404
    case ServerError = 500
    case UnprocessableEntity = 422
}

struct model {
    
    static let type = model()
    
    let MainDataEntity = "MainDataEntity"
}
