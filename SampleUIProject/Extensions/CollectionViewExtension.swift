//
//  CollectionViewExtension.swift
//  sampleProject
//
//  Created by Ragul on 14/01/20.
//  Copyright © 2020 Ragul. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func register(nibName: String) {
        let Nib = UINib(nibName: nibName, bundle: nil)
        register(Nib, forCellWithReuseIdentifier: nibName)
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
