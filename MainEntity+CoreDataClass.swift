//
//  MainEntity+CoreDataClass.swift
//  
//
//  Created by Ragul on 15/01/20.
//
//

import Foundation
import CoreData

@objc(MainEntity)
public class MainEntity: NSManagedObject {
    class func fetchRequest(for itemID: Int64) -> MainEntity {
        
        let viewContext = DataBaseManager.shared.context
        
        let request = MainEntity.fetchRequest() as NSFetchRequest
        
        let predicate = NSPredicate(format: "%K = %d", "id", itemID)
        
        request.predicate = predicate
        
        request.fetchLimit = 1
        
        let fetchResults = try! viewContext.fetch(request)
        
        if fetchResults.isEmpty {
            let user = NSEntityDescription.insertNewObject(forEntityName: MainEntity.entityName(), into: viewContext) as! MainEntity
            user.id = itemID
            return user
        } else {
            return fetchResults.first! 
        }
    }
    
    func populateWithRepresentation(representation: MainDataEntity) {
        
        self.id = representation.id ?? 0
        self.title = representation.title
        self.body = representation.body
        self.number = representation.number ?? 0
        self.state = representation.state
        self.login = representation.user?.login
        self.created_at = representation.created_at
    }
}
